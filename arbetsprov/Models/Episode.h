//
//  Episode.h
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Episode : NSObject

@property (nonatomic) NSNumber* id;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* summary;
@property (nonatomic) NSNumber* season;
@property (nonatomic) NSNumber* runtime;
@property (nonatomic) NSNumber* number;

-(nullable instancetype) initWith: (NSDictionary *) jsonObj;

@end

NS_ASSUME_NONNULL_END
