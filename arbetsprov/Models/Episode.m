//
//  Episode.m
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import "Episode.h"

@implementation Episode

-(nullable instancetype) initWith:(NSDictionary *)jsonObj {
  self = [self init];
  
  if (self) {
    
    NSNumber *id = jsonObj[@"id"];
    NSString *name = jsonObj[@"name"];
    NSString *summary = jsonObj[@"summary"];
    NSNumber *season = jsonObj[@"season"];
    NSNumber *runtime = jsonObj[@"runtime"];
    NSNumber *number = jsonObj[@"number"];
    
    if (id != nil &&
        name != nil &&
        summary != nil &&
        season != nil &&
        runtime != nil &&
        number != nil
        ) {
      
      self.id = id;
      self.name = name;
      self. summary = summary;
      self.season = season;
      self.runtime = runtime;
      self.number = number;
      
      return self;
    }
  }
  
  return nil;
}

@end
