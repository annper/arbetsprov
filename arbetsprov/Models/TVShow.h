//
//  TVShow.h
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TVShow : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *summary;

-(nullable instancetype) initWith: (NSDictionary *) jsonObj;

@end

NS_ASSUME_NONNULL_END
