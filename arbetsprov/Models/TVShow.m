//
//  TVShow.m
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import "TVShow.h"

@implementation TVShow

- (nullable instancetype)initWith:(NSDictionary *)jsonObj {
  self = [self init];
  
  if (self) {
    NSString *title = jsonObj[@"name"];
    NSString *summay = jsonObj[@"summary"];
    
    if (title != nil &&
        summay != nil) {
      self.title = title;
      self.summary = summay;
      
      return self;
    }
  }
  
  return nil;
}

@end
