//
//  main.m
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
