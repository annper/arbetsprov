//
//  ViewController.m
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import "ViewController.h"
#import "DetailViewController.h"
#import "WebService.h"
#import "NSString+DecodeHTML.h"
#import "TVShow.h"
#import "Episode.h"

@interface ViewController ()

@property (nonatomic) NSArray *data;
@property (nonatomic) CGFloat topViewHeight;

@property (nonatomic) UIView *topView;
@property (nonatomic) UITableView *tableView;

@property (nonatomic) UIView * loadingView;

@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [self setupTopView];
  [self setupTableView];
  [self addLoadingView];
  [self downloadDataAndRender];

}

- (void) setupTopView {
  self.topViewHeight = 200;
  CGRect frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, self.topViewHeight);
  self.topView = [[UIView alloc] initWithFrame:frame];
  self.topView.backgroundColor = UIColor.grayColor;
  
  [self.view addSubview:self.topView];
}

- (void) setupTableView {
  self.tableView = [[UITableView alloc] init];

  // Configure table view
  self.tableView.delegate = self;
  self.tableView.dataSource = self;
  self.tableView.tableFooterView = [[UIView alloc] init];
  [self.tableView registerClass:UITableViewCell.self forCellReuseIdentifier:@"Cell"];
  
  [self.view addSubview:self.tableView];
  
  // Add constraints
  self.tableView.translatesAutoresizingMaskIntoConstraints = NO;

  NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
  NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
  NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:0];
  NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
  
  [self.tableView.superview addConstraint:top];
  [self.tableView.superview addConstraint:leading];
  [self.tableView.superview addConstraint:trailing];
  [self.tableView.superview addConstraint:bottom];
}

- (void) downloadDataAndRender {
  
  NSString* url = @"https://api.tvmaze.com/singlesearch/shows?q=black-mirror&embed=episodes";
  
  [WebService downloadFromUrlAndParseData:url completionHandler:^(TVShow * _Nullable tvShow, NSArray * _Nullable episodes, NSError * _Nullable error) {
    
    if (episodes != nil) {
      self.data = episodes;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.tableView reloadData];
      self.title = tvShow.title;
      if (tvShow != nil) {
        [self setupSummaryLabelWithText:tvShow.summary];
      }
      
      [self removeLoadingView];
    });
    
  }];
  
}

-(void) setupSummaryLabelWithText: (NSString *) htmlText {
  NSAttributedString *decodedText = [htmlText decodeHTML];
  
  // Confgire the label
  UILabel *label = [[UILabel alloc] init];
  label.translatesAutoresizingMaskIntoConstraints = NO;
  label.numberOfLines = 0;
  label.lineBreakMode = NSLineBreakByWordWrapping;
  label.textAlignment = NSTextAlignmentCenter;
  [label setTextColor:UIColor.whiteColor];
  [label setAttributedText:decodedText];
  [label setMinimumScaleFactor:0.1];
  label.adjustsFontSizeToFitWidth = YES;
  
  [label sizeToFit];
  
  [self.topView addSubview:label];
  
  // Add constraints
  NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeTop multiplier:1.0 constant:15];
  NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:15];
  NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-15];
  NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-15];
  
  [label.superview addConstraint:top];
  [label.superview addConstraint:leading];
  [label.superview addConstraint:trailing];
  [label.superview addConstraint:bottom];
  
}

- (void) addLoadingView {
  CGRect frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height);
  self.loadingView = [[UIView alloc] initWithFrame:frame];
  self.loadingView.backgroundColor = UIColor.grayColor;
  self.loadingView.alpha = 0.5;
  
  UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
  activityIndicator.center = self.loadingView.center;
  [activityIndicator startAnimating];
  [self.loadingView addSubview:activityIndicator];
  [self.view addSubview:self.loadingView];
}

- (void) removeLoadingView {
  [self.loadingView removeFromSuperview];
}

// MARK: - UITableViewDelegate & UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
  
  // Configure the cell
  Episode *episode = [[Episode alloc] initWith:self.data[indexPath.row]];
  if (episode != nil) {
    cell.textLabel.text = episode.name;
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
  }
  
  return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.data.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
  Episode *episode = [[Episode alloc] initWith:self.data[indexPath.row]];

  if (episode != nil){
    DetailViewController *detailViewController = [[DetailViewController alloc] initWith:episode];
    [self.navigationController pushViewController:detailViewController animated:YES];
  }
}

@end
