//
//  DetailViewController.h
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class Episode;

@interface DetailViewController : UIViewController

- (instancetype) initWith: (Episode *) episode;

@end

NS_ASSUME_NONNULL_END
