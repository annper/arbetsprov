//
//  DetailViewController.m
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import "DetailViewController.h"
#import "Episode.h"
#import "NSString+Highlight.h"
#import "NSString+DecodeHTML.h"

@interface DetailViewController ()

@property (nonatomic) CGFloat fontSize;
@property (nonatomic) UIFont *mainFont;
@property (nonatomic) UIFont *boldFont;

@property (nonatomic) UILabel *seasonLabel;
@property (nonatomic) UILabel *episodeLabel;
@property (nonatomic) UILabel *summaryLabel;
@property (nonatomic) UILabel *runtimeLabel;

@property (nonatomic) Episode *episode;

@end

@implementation DetailViewController


- (void)viewDidLoad {
  [super viewDidLoad];
  [self setupView];
}


- (instancetype) initWith:(Episode *)episode {
  self = [self init];
  
  if (self) {
    self.episode = episode;
    self.fontSize = 16;
    self.mainFont = [UIFont fontWithName:@"HelveticaNeue" size:self.fontSize];
    self.boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:self.fontSize];
  }
  
  return self;
}


- (void) setupView {
  self.title = self.episode.name;
  self.view.backgroundColor = UIColor.whiteColor;
  [self addSeasonLabel:self.episode.season];
  [self addEpisodeLabel:self.episode.number];
  [self addSummaryLabel:self.episode.summary];
  [self addRuntimeLabel:self.episode.runtime];
}

// MARK: - Setup labels

- (void) addSeasonLabel: (NSNumber *) season {
  self.seasonLabel = [self setupLabelWithTitle:@"Season" content:season];
  
  [self.view addSubview:self.seasonLabel];
  
  // Set constraints
  [self applyTopConstraintTo:self.seasonLabel topElement:self.view constraintTopTo:NSLayoutAttributeTop];
  [self applyConstraintsTo:self.seasonLabel];
  
}


- (void) addEpisodeLabel: (NSNumber *) episode {
  self.episodeLabel = [self setupLabelWithTitle:@"Episode" content:episode];
  
  [self.view addSubview:self.episodeLabel];
  
  // Set constraints
  [self applyTopConstraintTo:self.episodeLabel topElement:self.seasonLabel constraintTopTo:NSLayoutAttributeBottom];
  [self applyConstraintsTo:self.episodeLabel];
}


- (void) addSummaryLabel: (NSString *) summary {
  NSAttributedString *decodedText = [summary decodeHTML];
  
  // Set the summary font
  NSMutableAttributedString *mutableDecodedText = [[NSMutableAttributedString alloc] initWithAttributedString:decodedText];
  [mutableDecodedText addAttribute:NSFontAttributeName value:self.mainFont range:NSMakeRange(0, mutableDecodedText.length)];
  
  // Create the summary label with bold font
  NSAttributedString *summaryText = [[NSAttributedString alloc] initWithString:@"Summary: " attributes:@{NSFontAttributeName: self.boldFont}];
  NSMutableAttributedString *mutableSummaryText = [[NSMutableAttributedString alloc] initWithAttributedString:summaryText];
  
  // Create a label from the label and the text
  [mutableSummaryText appendAttributedString:mutableDecodedText];
  self.summaryLabel = [self createLabelWithText:mutableSummaryText];
  
  [self.view addSubview:self.summaryLabel];
  
  // Set constraints
  [self applyTopConstraintTo:self.summaryLabel topElement:self.episodeLabel constraintTopTo:NSLayoutAttributeBottom];
  [self applyConstraintsTo:self.summaryLabel];
}

- (void) addRuntimeLabel: (NSNumber *) runtime {
  self.runtimeLabel = [self setupLabelWithTitle:@"Runtime" content:runtime];
  [self.view addSubview:self.runtimeLabel];
  
  // Set constraints
  [self applyTopConstraintTo:self.runtimeLabel topElement:self.summaryLabel constraintTopTo:NSLayoutAttributeBottom];
  [self applyConstraintsTo:self.runtimeLabel];
  
}

// MARK: - Utility

- (UILabel *) setupLabelWithTitle: (NSString *) title content:(id) content {
  
  NSString *fullText = [NSString stringWithFormat:@"%@: %@", title, content];
  
  NSAttributedString *attrString = [fullText highlight:[NSArray arrayWithObject:title] baseFont:self.mainFont highlightFont:self.boldFont];
  
  return [self createLabelWithText:attrString];
}


- (UILabel *) createLabelWithText: (NSAttributedString *) text {
  
  UILabel *label = [[UILabel alloc] init];
  label.translatesAutoresizingMaskIntoConstraints = NO;
  
  [label setAttributedText:text];
  [label setTextColor:UIColor.blackColor];
  label.numberOfLines = 0;
  label.lineBreakMode = NSLineBreakByWordWrapping;
  label.textAlignment = NSTextAlignmentLeft;
  
  return label;
}


- (void) applyConstraintsTo: (UILabel *) label {
  NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:15];
  NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:-15];
  
  [label.superview addConstraint:leading];
  [label.superview addConstraint:trailing];
}


- (void) applyTopConstraintTo: (UILabel *) label topElement: (UIView *) topElement constraintTopTo: (NSLayoutAttribute) topAttribute {
  
  NSLayoutConstraint *topMargin = [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:topElement attribute:topAttribute multiplier:1.0 constant:15];
  
  [label.superview addConstraint:topMargin];
  
}
@end
