//
//  ViewController.h
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@end

