//
//  WebService.h
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class TVShow;
@class Episode;

@interface WebService : NSObject

+ (void)downloadFromUrlAndParseData:(NSString *)urlString completionHandler:(void (^)(TVShow * _Nullable tvShow, NSArray * _Nullable episodes, NSError * _Nullable error))finished;

@end

NS_ASSUME_NONNULL_END
