//
//  WebService.m
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import "WebService.h"
#import "TVShow.h"
#import "Episode.h"

@implementation WebService

+ (void)downloadFromUrlAndParseData:(NSString *)urlString completionHandler:(void (^)(TVShow * _Nullable, NSArray * _Nullable, NSError * _Nullable))finished {
  
  NSURL* url = [[NSURL alloc] initWithString:urlString];
  NSURLSession* session = [NSURLSession sharedSession];
  
  NSURLSessionDataTask* dataTask = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
    
    // Make sure there is data
    if (data == nil) { return finished(nil, nil, error); }
    
    // Convert data to json
    NSDictionary* jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    if (jsonObject == nil) { return finished(nil, nil, error); }
    
    // Parse and return the data
    TVShow *tvShow = [[TVShow alloc] initWith:jsonObject];
    
    NSDictionary* embedded = jsonObject[@"_embedded"];
    NSArray *episodes = embedded[@"episodes"];
    if (episodes == nil) { return finished(tvShow, nil, error);}
    
    finished(tvShow, episodes, nil);
  }];
  
  [dataTask resume];
  
}

@end
