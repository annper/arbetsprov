//
//  NSString+DecodeHTML.m
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import "NSString+DecodeHTML.h"

@implementation NSString (NSString_DecodeHTML)


- (NSAttributedString * _Nullable)decodeHTML {
  NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
  if (data == nil) { return nil; }
  
  NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:data options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                                 NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding),} documentAttributes:nil error:nil];
  // Remove any whitespace
  NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
  NSString *trimmed = [attributedString.string stringByTrimmingCharactersInSet:whitespace];
  
  return [[NSAttributedString alloc] initWithString:trimmed];
}

@end
