//
//  NSString+DecodeHTML.h
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (NSString_DecodeHTML)

- (NSAttributedString * _Nullable) decodeHTML;

@end

NS_ASSUME_NONNULL_END
