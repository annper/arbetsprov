//
//  NSString+Highlight.m
//  arbetsprov
//
//  Created by Annie Persson on 19/12/2018.
//  Copyright © 2018 Annie Persson. All rights reserved.
//

#import "NSString+Highlight.h"

@implementation NSString (NSString_Highlight)

- (NSAttributedString *)highlight:(NSArray *)strings baseFont:(UIFont *)baseFont highlightFont:(UIFont *)highlightFont {
  
  NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:self];
  
  // Apply the base font on the full string
  [mutableString addAttribute:NSFontAttributeName value:baseFont range:NSMakeRange(0, mutableString.length)];
  
  // Highlight the string from the strings array by applying the highlight font
  for (NSString *string in strings) {
    NSRange highlightRange = [mutableString.mutableString rangeOfString:string];
    [mutableString addAttribute:NSFontAttributeName value:highlightFont range:highlightRange];
  }
  
  return [[NSAttributedString alloc] initWithAttributedString:mutableString];
}

@end
